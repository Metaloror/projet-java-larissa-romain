package modeleFenetre;

import java.awt.Color;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
/**
 * Classe de l'interface graphique intervenant lors du choix du client 
 * @author Larissa AFFOLABI
 * @version 1.3
 */
public class ChoixClient extends JPanel implements ActionListener {
	/**
	 * numero de version pour classe serialisable Permet d'eviter le warning
	 * "The serializable class ArticleFenetre does not declare a static final serialVersionUID field of type long"
	 */
	private static final long serialVersionUID = 1L; 
	/**
	 * bouton d�signant un nouveau client 
	 */
	private JButton boutonNewClt;
	
	/**
	 * bouton d�signant un client existant 
	 */
	private JButton boutonExtClt ;
	/**
	 * attribut de de la fen�tre principale
	 */
	private GMAOFenetre g ;
	/**
	 * attribut de la fen�tre se saisie client 
	 */
	private SaisirClient clt ;
	
    /**
     * Constructeur
     * @param g la fen�tre 
     */
	public ChoixClient( GMAOFenetre g){
		this.g = g ;
		
		// instanciation des composants graphiques 
		boutonNewClt=new JButton("Nouveau client");
		boutonExtClt=new JButton("Client existant");
		
		setLayout(null);
		
		// changement de couleur de l'arri�re-plan 
		setBackground(new Color(130, 151, 213));
		
		// Dimensionnement des composants graphiques 
		boutonNewClt.setBounds (80, 80, 200, 50) ;
		boutonExtClt.setBounds (80, 150, 200, 50) ;
		
		// ajout des composants graphiques 
		add(boutonNewClt) ;
		add(boutonExtClt) ; 
		
		// ajout des �couteurs aux boutons de l'interface 
		boutonNewClt.addActionListener(this);
		boutonExtClt.addActionListener(this);
		
		
		
	}	

	/**
	 * Permet de g�rer les diff�rentes actions sur les boutons 
	 */
	public void actionPerformed(ActionEvent e) {
		
		if (e.getSource()==boutonNewClt){
		   clt =new SaisirClient();
		}
		if (e.getSource()==boutonExtClt){
			g.switchPanel() ;
		}
			}
	

}
