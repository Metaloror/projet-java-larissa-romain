package modeleFenetre;

import java.awt.* ;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.* ;
/**
 * classe d'interface de la fen�tre principale 
 * @author Larissa AFFOLABI
 * @version 1.5
 */
public class GMAOFenetre extends JFrame implements ActionListener {
	
	/**
	 * numero de version pour classe serialisable Permet d'eviter le warning
	 * "The serializable class ArticleFenetre does not declare a static final serialVersionUID field of type long"
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * conteneur : il accueille les differents composants graphiques de
	 * la Fenetre
	 */
	private JPanel containerPanel; 
	
    /**
     * le label d'introduction dans la page d'accueil 
     */
	private JLabel labelIntro ;
	/**
	 * bouton qui indique qu'il s'agit d'un Client 
	 */
	private JButton boutonClient  ;

	/**
	 * bouton qui indique qu'il s'agit d'un employ� de l'entreprise 
	 */
	private JButton boutonEmploye ;
	
	
	/**
	 * Constructeur D�finit la fen�tre et ses composants - affiche la fen�tre
	 */
	public GMAOFenetre() {
	
		// on fixe le titre de la fen�tre
		this.setTitle("Logiciel GMAO ");
		
		// initialisation de la taille de la fen�tre
		this.setSize(400, 400);

		// cr�ation du conteneur
		containerPanel = new JPanel( );
		
		// choix du Layout pour ce conteneur
		// il permet de g�rer la position des �l�ments
		// il autorisera un retaillage de la fen�tre en conservant la
		// pr�sentation
		containerPanel.setLayout(new BoxLayout(containerPanel, BoxLayout.PAGE_AXIS));

		// choix de la couleur pour le conteneur
		containerPanel.setBackground(new Color(130, 151, 213));
		
		// instantiation des composants graphiques
		boutonClient= new JButton(" Client ") ;
		boutonEmploye= new JButton(" Employe ") ;
		labelIntro = new JLabel (" Vous �tes un: ") ;
		
		labelIntro.setFont(new Font("Vous �tes un:", Font.BOLD, 28));
		containerPanel.add(labelIntro);
		containerPanel.add(Box.createRigidArea(new Dimension(0, 20)));
		
		//ajout des �l�ments du panel  
		
		boutonClient.setMaximumSize(new Dimension(200, 50));
		containerPanel.add(Box.createRigidArea(new Dimension(10, 20)));
		containerPanel.add(boutonClient);
        
		boutonEmploye.setMaximumSize(new Dimension(200, 50));
		containerPanel.add(Box.createRigidArea(new Dimension(0, 20)));
		containerPanel.add(boutonEmploye);
		
		// ajouter une bordure vide de taille constante autour de l'ensemble des
		// composants
		containerPanel.setBorder(BorderFactory
				.createEmptyBorder(10, 80, 10, 10));
  
		// ajout des �couteurs sur les boutons pour g�rer les �v�nements
		boutonClient.addActionListener(this);
		boutonEmploye.addActionListener(this);

		// permet de quitter l'application si on ferme la fen�tre
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		this.setContentPane(containerPanel);
		this.setLocationRelativeTo(null);
		
        // fixe la taille de la fen�tre 
		this.setResizable(false) ;
		
		// affichage de la fen�tre
		this.setVisible(true);
	}

	/**
	 * G�re les actions r�alis�es sur les boutons
	 *
	 */
	public void actionPerformed(ActionEvent ae) {
		
	 		  if (ae.getSource() == boutonClient) {
				// on cr�e l'objet message
				ChoixClient clt = new ChoixClient(this);
				this.setContentPane(clt);
				
				//rafra�chissement du panel 
				this.revalidate();
			   } 
			  else if (ae.getSource() == boutonEmploye) {
				 Connexion con= new Connexion() ;
				 setContentPane(con);
				 
				//rafra�chissement du panel 
					this.revalidate(); 
			  }

	}

   public void switchPanel () { 
		this.setContentPane(new ConnexionClient1() );
		this.revalidate() ;
	}
   
	
}
