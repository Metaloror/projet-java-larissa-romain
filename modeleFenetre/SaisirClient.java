package modeleFenetre;

import modeleDAO.* ;
import modele.* ;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
/**
 * classe fen�tre de la saisie de client 
 * @author Larissa AFFOLABI
 *
 */
public class SaisirClient extends JFrame implements ActionListener {
	/**
	 * numero de version pour classe serialisable Permet d'eviter le warning
     * "The serializable class ArticleFenetre does not declare a static final serialVersionUID field of type long"
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * conteneur : il accueille les differents composants graphiques de
	 * la Fenetre
	 */
	private JPanel containerPanel;
	/**
	 * label du nom de l'entreprise
	 */
	private JLabel labelNom ;
	/**
	 * label du code ape 
	 */
	private JLabel labelApe ;
	/**
	 * label du num�ro de siret 
	 */
	private JLabel labelSiret ;
	/**
	 * label de l'adresse 
	 */
	private JLabel labelAdresse ; 
	/**
	 * label de l'adresse �lectronique 
	 */
	private JLabel labelMail ;
	/**
	 * label du num�ro de t�l�phone 
	 */
	private JLabel labelPhone ;
	
	/**
	 * zone texte pour la saise du nom de l'entreprise 
	 */
	private JTextField zoneTextEntreprise=new JTextField();
	/**
	 * zone texte pour la saise du code APE
	 */
	private JTextField zoneTextApe=new JTextField();
	/**
	 * zone texte pour la saise du num�ro de siret  
	 */
	private JTextField zoneTextSiret=new JTextField();
	/**
	 * zone texte pour l'adresse
	 */
	private JTextField zoneTextAdresse=new JTextField();
	/**
	 * zone texte pour la saise du mail
	 */
	private JTextField zoneTextMail=new JTextField();
	/**
	 * zone texte pour la saise du num�ro de t�l�phone 
	 */
	private JTextField zoneTextTelephone=new JTextField();
	
	/**
	 * bouton enregistrer pour enregistrer le client 
	 */
	private JButton bouton ;
	
	/**
	 * constructeur 
	 */
	public SaisirClient(){
		
		// on fixe le titre de la fen�tre
				this.setTitle(" Enregistrement d'un nouveau client  ");
				
		// initialisation de la taille de la fen�tre
				this.setSize(800, 500);

		// cr�ation du conteneur 
				containerPanel = new JPanel();
				
		/* choix du Layout pour ce conteneur
	    il permet de g�rer la position des �l�ments
	    il autorisera un retaillage de la fen�tre en conservant la
		pr�sentation
		 */
		containerPanel.setLayout( new FlowLayout() );

		// choix de la couleur pour le conteneur
			containerPanel.setBackground(new Color(130, 151, 213));
						
			
       // instanciation des composants 
			
				labelNom =new JLabel("Nom Entreprise");
			    labelApe =new JLabel("Code APE");
				labelSiret =new JLabel("Num�ro SIRET");
				labelAdresse =new JLabel("Adresse"); 
				labelMail =new JLabel("eMail");
			    labelPhone =new JLabel("N� Telephone");
			    
			    zoneTextEntreprise=new JTextField();
			    zoneTextApe=new JTextField();
			    zoneTextSiret=new JTextField();
			    zoneTextAdresse=new JTextField();
			    zoneTextMail=new JTextField();
			    zoneTextTelephone=new JTextField();
			    
			    containerPanel.setLayout( new GridLayout(4,5, 10, 80) ) ;
			    
			    bouton =new JButton(" Enregistrer ") ;
			    
	 //ajout des �l�ments du panel  
			    
			    labelNom.setHorizontalAlignment(JLabel.CENTER);
				containerPanel.add(labelNom ) ;
				containerPanel.add(zoneTextEntreprise) ;
				
				labelApe.setHorizontalAlignment(JLabel.CENTER);
				containerPanel.add(labelApe ) ;
				containerPanel.add(zoneTextApe) ;
				
				labelAdresse.setHorizontalAlignment(JLabel.CENTER); 
				containerPanel.add(labelAdresse) ;
				containerPanel.add(zoneTextAdresse) ;
				
				labelPhone.setHorizontalAlignment(JLabel.CENTER);
				containerPanel.add(labelPhone) ;
				containerPanel.add(zoneTextTelephone) ;
				
				labelSiret.setHorizontalAlignment(JLabel.CENTER);
				containerPanel.add(labelSiret ) ;
				containerPanel.add(zoneTextSiret) ;
				
				labelMail.setHorizontalAlignment(JLabel.CENTER);
				containerPanel.add(labelMail) ;
				containerPanel.add(zoneTextMail) ;
				
				containerPanel.add(new JLabel());
				containerPanel.add(new JLabel());
				containerPanel.add(bouton) ;
				bouton.addActionListener(this);
				
				containerPanel.setBorder(BorderFactory.createEmptyBorder(50, 20, 30, 20));
				
			    
				// permet de quitter l'application si on ferme la fen�tre
				//this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

				this.setContentPane(containerPanel);

				// affichage de la fen�tre
				this.setVisible(true) ;
				
				
		
	}

	/**
	 * G�re les actions r�alis�es sur les boutons
	 *
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		
		if (e.getSource() == bouton) {
			// on cr�e l'objet message
		 ClientDAO cltDAO = new ClientDAO () ;
		 
			Client clt = new Client( zoneTextEntreprise.getText(), zoneTextAdresse.getText(), Integer.parseInt(zoneTextSiret.getText()),
			 zoneTextApe.getText(), zoneTextMail.getText(), Integer.parseInt( zoneTextTelephone.getText() ) );
	    
			if (cltDAO.ajouter(clt) != 1) 
				JOptionPane.showMessageDialog(this,
					"Une erreur s'est produite lors de l'enregistrement", "Erreur",
					JOptionPane.ERROR_MESSAGE);
			else{
				this.setVisible(false);
				this.dispose();
				JOptionPane.showMessageDialog(this,
						" Nouveau client enregistr�", "Ajout Effectu�",
						JOptionPane.OK_OPTION);
			}
	  
	 } 
		
	}
 
 
}
