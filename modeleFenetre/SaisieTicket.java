package modeleFenetre;
import modele.* ;

import modeleDAO.* ;
import javax.swing.*; 
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
/**
 * classe interface saisie de ticket 
 * @author Larissa AFFOLABI
 * @version 1.2
 */
public class SaisieTicket extends JFrame implements ActionListener  {

	/**
	 * numero de version pour classe serialisable Permet d'eviter le warning
     * "The serializable class ArticleFenetre does not declare a static final serialVersionUID field of type long"
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * conteneur : il accueille les differents composants graphiques de
	 * la Fenetre
	 */
	private JPanel containerPanel;
	/**
	 * le client connect� 
	 */
	private Client user ;
	/**
	 * le label du nom de la maintenance 
	 */
	private JLabel labelNomMtn;
	/**
	 * le label du type de maintenance 
	 */
	private JLabel labelTypeMtn ;
	/**
	 * le label du lieu de la maintenance 
	 */
	private JLabel labelLieu;
	/**
	 * le label de la description de la maintenance
	 */
	private JLabel labelDescription ;
	/**
	 * zone de texte du nom de la demande 
	 */
	private JTextField zoneNomDemande ;
	/**
	 * zone de texte du lieu de maintenance 
	 */
	private JTextField zoneLieu ;
	/**
	 * zone de texte de la description de la maintenance 
	 */
	private JTextField zoneDescription ;
	/**
	 * bouton validant la cr�ation d'un ticket 
	 */
	private JButton boutonCreer ;
	/**
	 * liste des types de maintenance 
	 */
	private String[] typesMaintenance = new String[] {"Corrective ","Paliative"," Pr�ventive"};
    /**
     * jcombo box pour la liste des types de maintenance 
     */
    private JComboBox<String> typesMtn ;

    
	public SaisieTicket(Client user){
		
		this.user=user ;
		// on fixe le titre de la fen�tre
		this.setTitle(" Saisie de ticket  ") ;
		
        // initialisation de la taille de la fen�tre
		this.setSize(500, 400);

      // cr�ation du conteneur 
		containerPanel = new JPanel();
		
     /* choix du Layout pour ce conteneur
       il permet de g�rer la position des �l�ments
       il autorisera un retaillage de la fen�tre en conservant la
       pr�sentation
     */
     containerPanel.setLayout( new FlowLayout() );

    // choix de la couleur pour le conteneur
	containerPanel.setBackground(new Color(50, 205,182));
	
      // instanciation des composants 
		 
	typesMtn = new JComboBox<String>(typesMaintenance);
	labelNomMtn=new JLabel("Nom demande") ;
	labelTypeMtn =new JLabel("Type Maintenance");
	labelLieu=new JLabel("Lieu");
	labelDescription =new JLabel("Description");
	zoneNomDemande=new JTextField(); 
	zoneLieu=new JTextField();
	zoneDescription=new JTextField();
	boutonCreer =new JButton("Cr�er ticket");
	
	containerPanel.setLayout( new GridLayout(3 ,5, 10, 80) ) ;
	
	typesMtn.setPreferredSize(new Dimension( 150, 50));
	typesMtn.setBackground( new Color(255, 255,255) );
	
	// ajout des composants graphiques au contenu du panel 
	
	this.setResizable(false) ;
	
	labelNomMtn.setHorizontalAlignment(JLabel.CENTER);
	containerPanel.add(labelNomMtn) ;
	containerPanel.add(zoneNomDemande) ;
	
	labelTypeMtn.setHorizontalAlignment(JLabel.CENTER);
	containerPanel.add(labelTypeMtn ) ;
	containerPanel.add(typesMtn) ;
	
	labelLieu.setHorizontalAlignment(JLabel.CENTER); 
	containerPanel.add(labelLieu) ;
	containerPanel.add(zoneLieu) ;
	
	labelDescription.setHorizontalAlignment(JLabel.CENTER);
	containerPanel.add(labelDescription) ;
	containerPanel.add(zoneDescription) ;
	
	containerPanel.add(new JLabel());
	containerPanel.add(new JLabel());
	containerPanel.add(boutonCreer);
	boutonCreer.addActionListener(this);
	
	containerPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 20));
	
	
	
	this.setContentPane(containerPanel);
	this.setVisible(true); 	
	}
	
  /**
   * g�re les actions sur les boutons 
   */
public void actionPerformed(ActionEvent e) {
		
  if (e.getSource() == boutonCreer) {
			 
		Ticket tkt = new Ticket( zoneNomDemande.getText() , user.getId(), 
				zoneDescription.getText(), zoneLieu.getText(), (String) typesMtn.getSelectedItem()) ;
			TicketDAO tktDAO = new TicketDAO() ;
			
			 if (  tktDAO.ajouter( tkt )  != 1) 
					JOptionPane.showMessageDialog(this,
						"Une erreur s'est produite lors de l'enregistrement", "Erreur",
						JOptionPane.ERROR_MESSAGE);
				else {
					this.setVisible(false);
					this.dispose();
					JOptionPane.showMessageDialog(this,
							" Ticket de maintenance cr��", "Ajout Effectu�",
							JOptionPane.OK_OPTION);
				}
				
		}
		
	}
	

}
