package modeleFenetre ;
import modeleDAO.* ;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class ConnexionClientFenetre extends JPanel implements ActionListener {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * label du num�ro de siret 
	 */
	private JLabel labelSiret =new JLabel(" Num�ro de siret ");
	/**
	 * zone de texte du num�ro de siret 
	 */
	private JTextField zoneTextSiret ;
	/**
	 * bouton de connexion 
	 */
	JButton boutonConnexion ;
	
	public ConnexionClientFenetre(){
		
		// instanciation  des composants graphiques 
		
		boutonConnexion = new JButton("Connecter")  ;
		zoneTextSiret = new JTextField(); 
		
		setLayout(new  GridLayout( 4, 2));
		
		setBackground(new Color(130, 151, 213));
		
		add(Box.createRigidArea(new Dimension(0, 10)));
		
		labelSiret.setFont(new Font(" Num�ro de Siret:", Font.BOLD, 15));
		add(labelSiret);
		add(zoneTextSiret) ;
		add(Box.createRigidArea(new Dimension(0, 20)));
		add(boutonConnexion) ;
		 
	     
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		
		
		if (e.getSource()== boutonConnexion ){
			FenetreClient fen = new FenetreClient() ;
			ConnexionClientDAO conDAO = new ConnexionClientDAO() ;
			
			if(conDAO.connecter(Integer.parseInt(zoneTextSiret.getText())) != null ) {
				
			fen.setUser( conDAO.connecter( Integer.parseInt(  zoneTextSiret.getText() ) ) ) ;
			 ;
			}
			else
				JOptionPane.showMessageDialog(this,
						" Num�ro de siret introuvable, revoyez votre saisie .", "Erreur",
						JOptionPane.ERROR_MESSAGE);
			
		}
		
	}
	
	

}
