package modeleFenetre;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import modele.Client;
import modeleDAO.ClientDAO;
public class ClientFenetre extends JFrame implements ActionListener {
	
	/**
	 * numero de version pour classe serialisable Permet d'eviter le warning
	 * "The serializable class ArticleFenetre does not declare a static final serialVersionUID field of type long"
	 */
	private static final long serialVersionUID = 1L;
	
	private ClientDAO clientDAO ;
	
	Container contenu;
	JLabel L2=new JLabel("Nom Entreprise");
	JLabel L5=new JLabel("Code APE");
	JLabel L4=new JLabel("Num�ro SIRET");
	JLabel L3=new JLabel("Adresse");
	JLabel L1=new JLabel("Saisie du client");
	JLabel L6=new JLabel("eMail");
	JLabel L7=new JLabel("N�Telephone");
	
	JTextField entreprise=new JTextField();
	JTextField siret=new JTextField();
	JTextField adresse=new JTextField();
	JTextField ape=new JTextField();
	JTextField mail=new JTextField();
	JTextField telephone=new JTextField();
	
	
	JButton bouton=new JButton("Ajouter");
	public ClientFenetre(){
		this.setBounds(100,100,600,400);
		this.setTitle("saisie du client");
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		contenu=this.getContentPane();
		contenu.setLayout(null);
		contenu.add(L1);
		contenu.add(L2);
		contenu.add(L3);
		contenu.add(L4);
		contenu.add(L5);
		contenu.add(L6);
		contenu.add(L7);
		
		contenu.add(entreprise);
		contenu.add(siret);
		contenu.add(adresse);
		contenu.add(ape);
		contenu.add(mail);
		contenu.add(telephone);
		contenu.add(bouton);
		
		L5.setBounds(250,0,100,50);
		
		L1.setBounds(40,105,100,50);
		entreprise.setBounds(150,120,100,20);
		
		L2.setBounds(45,165,100,50);
		ape.setBounds(350,120,100,20);
		
		L3.setBounds(255,105,100,50);
		siret.setBounds(150,180,100,20);
		
		L4.setBounds(270,165,100,50);
		adresse.setBounds(350,180,100,20);
		
		L6.setBounds(60,45,100,50);
		mail.setBounds(150,60,100,20);
		
		L7.setBounds(260,45,100,50);
		telephone.setBounds(350,60,100,20);
		
		bouton.setBounds(250,250,100,50) ;
		
		
		// ajout des �couteurs sur les boutons pour g�rer les �v�nements
				bouton.addActionListener(this);
	}
		
		/**
		 * G�re les actions r�alis�es sur les boutons
		 *
		 */
		public void actionPerformed(ActionEvent ae) {
			int retour; // code de retour de la classe ArticleDAO

			try {
				if (ae.getSource() == bouton) {
					// on cr�e l'objet message
					Client clt = new Client ( this.entreprise.getText(),
							this.adresse.getText(),
							Integer.parseInt(this.siret.getText()), 
							this.ape.getText(),  
							  this.mail.getText(), 
							  Integer.parseInt(this.telephone.getText() ) ) ;
										
					// on demande � la classe de communication d'envoyer l'article
					// dans la table article
					retour = clientDAO.ajouter(clt);
					// affichage du nombre de lignes ajout�es
					// dans la bdd pour v�rification
					System.out.println("" + retour + " ligne ajout�e ");
					if (retour == 1)
						JOptionPane.showMessageDialog(this, "article ajout� !");
					else
						JOptionPane.showMessageDialog(this, "erreur ajout article",
								"Erreur", JOptionPane.ERROR_MESSAGE);
				}
			
			}
			catch (Exception e) {
				JOptionPane.showMessageDialog(this,
						"Veuillez contr�ler vos saisies", "Erreur",
						JOptionPane.ERROR_MESSAGE);
				System.err.println("Veuillez contr�ler vos saisies");
			}
		}
	
		public static void main(String[] args) {
		new ClientFenetre();
	}
		
}
