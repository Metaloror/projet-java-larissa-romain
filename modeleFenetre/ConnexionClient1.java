package modeleFenetre ;
import modeleDAO.* ;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;


/**
 * Classe interface graphique pour la connexion du client 
 * @author Larissa AFFOLABI
 * @version 1.5
 */
public class ConnexionClient1 extends JPanel implements ActionListener {
	
	/**
	 *  numero de version pour classe serialisable Permet d'eviter le warning
	 * "The serializable class ArticleFenetre does not declare a static final serialVersionUID field of type long"
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * label du num�ro de siret 
	 */
	private JLabel labelSiret =new JLabel(" Num�ro de siret ");
	
	/**
	 * zone de texte du num�ro de siret 
	 */
	private JTextField zoneTextSiret ;
	/**
	 * bouton de connexion 
	 */
	private JButton boutonConnexion ;
	/**
	 * attribut de la fen�tre d'interface client 
	 */
	private FenetreClient fen ;
	
	ConnexionClientDAO conDAO;
	
	public ConnexionClient1() {
		
		// instanciation  des composants graphiques 
		boutonConnexion = new JButton("Connecter")  ;
		zoneTextSiret = new JTextField(); 
		conDAO = new ConnexionClientDAO() ;
		
        this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS)) ;
        
		// changement de couleur de l'arri�re-plan 
		this.setBackground(new Color(130, 151, 213)) ;
		
		add(Box.createRigidArea(new Dimension(0, 30))); // ajout d'un espace vide constant 
		
		// changement de la police du label 
		labelSiret.setFont(new Font(" Num�ro de Siret:", Font.BOLD, 20));
		
		// ajout des composants graphiques au panel 
		add(labelSiret);
		add(Box.createRigidArea(new Dimension(0, 30))); // ajout d'un espace vide constant 
		add(zoneTextSiret) ;
		add(Box.createRigidArea(new Dimension(0, 40))); // ajout d'un espace vide constant 
		add(boutonConnexion) ;
		
		add(Box.createRigidArea(new Dimension(5, 100)));  // ajout d'un espace vide constant 
		
		boutonConnexion.addActionListener(this);  // ajout des �couteurs 
		 
		setBorder(BorderFactory
				.createEmptyBorder(40, 80, 20, 10));
		 
	     
	}
	/**
	 * g�re les actions sur les boutons 
	 */
	public void actionPerformed(ActionEvent e) {
		
		
		if (e.getSource()== boutonConnexion ){
			
			if(conDAO.connecter(Integer.parseInt(zoneTextSiret.getText())) != null ) {
				
				 fen = new FenetreClient( conDAO.connecter( Integer.parseInt(  zoneTextSiret.getText() ) ) ) ;
				
			}
			
			else {
				JOptionPane.showMessageDialog(this,
						" Num�ro de siret introuvable, revoyez votre saisie .", "Erreur",
						JOptionPane.ERROR_MESSAGE);
			
		}
		
	}
	
	

	}

}