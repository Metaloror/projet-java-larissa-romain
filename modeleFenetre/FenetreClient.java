package modeleFenetre;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import modele.* ;
/**
 * classe de l'interface graphique du client 
 * @author Larissa AFFOLABI
 * @version 1.3
 */
public class FenetreClient extends JFrame implements ActionListener {
	
/**
 * numero de version pour classe serialisable Permet d'eviter le warning
 * "The serializable class ArticleFenetre does not declare a static final serialVersionUID field of type long"
 */
  private static final long serialVersionUID = 1L; 

/**
 * l'utilisateur courant	
 */
  private Client user = null;
/**
 * 	Panel 
 */
  private JPanel containerPanel ;
/**
 *  bouton permettant d'afficher tout les tickets en cours de traitement de l'utilisateur
 */
  private JButton boutonAfficherTickets ;
/**
 * bouton permettant de cr�er un ticket de maintenance 
 */
  private JButton boutonCreerTicket ;
/**
 * bouton permettant de g�rer les comptes rendus 
 */
  private JButton boutonGererCompteRendu ; 
/**
 * Zone de texte pour afficher les tickets
 */
  private JTextArea zoneTextListTicket;

/**
 * Zone de d�filement pour la zone de texte
 */
  private JScrollPane zoneDefilement;
/**
 * jlabel indiquant le client connect� 
 */
  private JLabel labelUser ;
 	
/**
 * constructeur 
 * @param user le client connect� 
 */
  public FenetreClient( Client user) {
	  
	  this.user = user ;
	  
	        // on fixe le titre de la fen�tre
			this.setTitle(" Interface Client ");
			
			// initialisation de la taille de la fen�tre
			this.setSize(800,800);
			
			// cr�ation du conteneur
			containerPanel = new JPanel();

			// choix du Layout pour ce conteneur
			// il permet de g�rer la position des �l�ments
			// il autorisera un retaillage de la fen�tre en conservant la
			// pr�sentation
			// BoxLayout permet par exemple de positionner les �lements sur une
			// colonne ( PAGE_AXIS )
			containerPanel.setLayout(new BoxLayout(containerPanel,
					BoxLayout.PAGE_AXIS));
			
			// choix de la couleur pour le conteneur
			
			containerPanel.setBackground(new Color(230, 160, 98));
	  
			// instanciation des �l�ments des composants graphiques 
			
			boutonAfficherTickets = new JButton(" Afficher tous les tickets ")  ;
			boutonCreerTicket = new JButton(" Ouverture d'un ticket de maintenance") ;  
			boutonGererCompteRendu = new JButton(" Validation des comptes rendus") ;
			
			labelUser = new JLabel (" Client connect�: "+user.getNomEntreprise()  ) ;
			
			zoneTextListTicket = new JTextArea(5, 10);
			zoneDefilement = new JScrollPane(zoneTextListTicket);
			zoneTextListTicket.setEditable(false);
			
			// introduction d'un espace constant
			containerPanel.add(Box.createRigidArea(new Dimension(0, 50)));
			
			// ajout des composants graphiques 
			
			containerPanel.add(Box.createHorizontalBox());
			labelUser.setFont(new Font(" Client connect�: \n \t "+user.getNomEntreprise() , Font.BOLD, 20));
			containerPanel.add(labelUser) ;
			
			containerPanel.add(Box.createRigidArea(new Dimension(0, 50)));
			containerPanel.add(boutonCreerTicket);
			containerPanel.add(Box.createRigidArea(new Dimension(0, 40)));
			
			containerPanel.add(Box.createRigidArea(new Dimension(0, 40)));
			containerPanel.add(boutonAfficherTickets);
			containerPanel.add(Box.createRigidArea(new Dimension(0, 40)));
			
			containerPanel.add(zoneDefilement);
			containerPanel.add(Box.createRigidArea(new Dimension(0, 40)));
			
			containerPanel.add(boutonGererCompteRendu) ;


			// ajout d'une bordure vide de taille constante autour de l'ensemble des
			// composants
			containerPanel.setBorder(BorderFactory
					.createEmptyBorder(10, 60, 25, 15));

			// ajout des �couteurs sur les boutons pour g�rer les �v�nements
			boutonCreerTicket.addActionListener(this);
			boutonAfficherTickets.addActionListener(this);

			// permet de quitter l'application si on ferme la fen�tre
			this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

			this.setContentPane(containerPanel);

			// affichage de la fen�tre
			this.setVisible(true);
			
			
	  
  }



/**
 * getter de l'attribut user 
 * @return user les informations sur le client connect� 
 */
  public Client getUser() {
		return user ;
	}
  
/**
 * setter de l'attribut user 
 * @param user
 */
	public void setUser(Client user) {
	this.user = user;
}

/**
 * g�re les actions sur les boutons 
 */
public void actionPerformed(ActionEvent e) {
	

	if (e.getSource() == boutonCreerTicket) {
		 new SaisieTicket(user) ;
		
	}
}

	
}
