package modeleFenetre;
import modeleDAO.* ;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
/**
 * classse de connexion d'un employe 
 * @author Larissa AFFOLABI
 * @version 1.4
 */
public class Connexion extends JPanel implements ActionListener  {
	/**
	 * numero de version pour classe serialisable Permet d'eviter le warning
	 * "The serializable class ArticleFenetre does not declare a static final serialVersionUID field of type long"
	 */
	private static final long serialVersionUID = 1L;
     
	/**
	 * label du login
	 */
	private JLabel labelLogin ;
	/**
	 * label du mot de passe 
	 */
	private JLabel labelMotDePasse ;
	
	/**
	 * zone de texte pour le champ login
	 */
	private JTextField zoneTextLogin ;
	/**
	 * zone de texte pour le champ mot de passe 
	 */
	private JTextField zoneTextMotDePasse ;
	/**
	 * le bouton valider 
	 */
	private JButton boutonValider ;
	/**
	 * l'attribut de type fen�tre 
	 */
	private FenetreResponsable fen ;
	
	/**
	 * constructeur 
	 */
	public 	Connexion(){
		
		
		this.setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS)) ;
		
		// changement de couleur de l'arri�re-plan
		this.setBackground(new Color(130, 151, 213)) ;
		
		// instantiation des composants graphiques 
		labelLogin=new JLabel("Login");
		labelMotDePasse=new JLabel("Mot de passe") ;
		
		zoneTextLogin =new JTextField() ;
		zoneTextMotDePasse=new JTextField() ;
		
		boutonValider=new JButton("Valider") ;
	
		// changement de la police du label 
		labelLogin.setFont(new Font(" Login ", Font.BOLD, 28));
		labelMotDePasse.setFont(new Font(" Mot de passe ", Font.BOLD, 28));
		
		// Dimensionnement de la taille du bouton 
		boutonValider.setMaximumSize(new Dimension(150, 300));
		
		// ajout des composants graphiques 
		this.add(labelLogin);
		this.add(zoneTextLogin);
		this.add(labelMotDePasse);
		this.add(zoneTextMotDePasse);
		
		add(Box.createRigidArea(new Dimension(0, 50)));  // ajout d'un espace vide constant 
		
		this.add(boutonValider);
		
		add(Box.createRigidArea(new Dimension(5, 100))); // ajout d'un espace vide constant
		
		boutonValider.addActionListener(this);  // ajout des �couteurs au bouton 
		
		// ajout d'une bordure vide de taille constante autour de l'ensemble des
					// composants
		setBorder(BorderFactory
				.createEmptyBorder(10, 80, 10, 10)); 
	
}
	/**
	 * g�re les actions sur les boutons 
	 */
	public void actionPerformed(ActionEvent ae) {
		
		if (ae.getSource() == boutonValider) {
			
			  AuthentificationDAO ath = new AuthentificationDAO() ; 
			 
			  if ( ath.authentification( zoneTextLogin.getText() , zoneTextMotDePasse.getText())==-2){
				  JOptionPane.showMessageDialog(this,
							" Login erron�, revoyez votre saisie .", "Erreur",
							JOptionPane.ERROR_MESSAGE);
			     }
			  
			  if (ath.authentification( zoneTextLogin.getText() , zoneTextMotDePasse.getText())==-1){
				  JOptionPane.showMessageDialog(this,
							" Mot de passe erron�, revoyez votre saisie .", "Erreur",
							JOptionPane.ERROR_MESSAGE);
			     }
			  
			  if ( ath.authentification( zoneTextLogin.getText() , zoneTextMotDePasse.getText())==1){
				
			 fen = new FenetreResponsable( ath.getRespo(zoneTextLogin.getText(), zoneTextMotDePasse.getText() ) ) ;
			
			  }
			  
		/*	  if ( ath.authentification( zoneTextLogin.getText() , zoneTextMotDePasse.getText())==2){
				 
			  }*/
			
			
			
	}

	}

	}