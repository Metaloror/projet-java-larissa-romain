package modeleFenetre ;
import modele.* ;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
/**
 * classe d'interface du responsable de maintenance 
 * @author Larissa AFFOLABI
 * @version 1.3
 */
public class FenetreResponsable extends JFrame implements ActionListener {

	/**
	 *  numero de version pour classe serialisable Permet d'eviter le warning
     * "The serializable class ArticleFenetre does not declare a static final serialVersionUID field of type long"
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * l'attribut de l'utilisateur courant 
	 */
	private User user ;
	/**
	 * le label du responsable connect� 
	 */
	private JLabel labelUser ; 
	/**
	 * le bouton permettant de traiter un ticket 
	 */
	private JButton boutonTrtTkt ;
	/**
	 * le bouton permettant de valider une maintenance 
	 */
	private JButton boutonVldSaisie ;
	/**
	 * le bouton permettant de cl�turer un ticket
	 */
	private JButton boutonCloTkt ;
	/**
	 * le bouton permettant de Modifier un devis 
	 */
	private JButton boutonModifDevis  ;
	/**
	 * le panel de la fen�tre 
	 */
	private JPanel  containerPanel ;
	
	public FenetreResponsable( User user  ){
		
		this.user=user; 
		
		// on fixe le titre de la fen�tre
		this.setTitle(" Interface Responsable ");
		
		// initialisation de la taille de la fen�tre
		this.setSize(600,600);
		
		// cr�ation du conteneur
		containerPanel = new JPanel();

		// choix du Layout pour ce conteneur
		// il permet de g�rer la position des �l�ments
		// il autorisera un retaillage de la fen�tre en conservant la
		// pr�sentation
		// BoxLayout permet par exemple de positionner les �lements sur une
		// colonne ( PAGE_AXIS )
		containerPanel.setLayout(new BoxLayout(containerPanel,
				BoxLayout.PAGE_AXIS));
		
		// choix de la couleur pour le conteneur
		containerPanel.setBackground(new Color(230, 160, 98));
		
		// instanciation des �l�ments des composants graphiques 
		
		boutonTrtTkt=new JButton(" Traitement ticket ");
		boutonVldSaisie=new JButton("Valider saisie Maintenance");
		boutonCloTkt=new JButton(" Cl�ture de ticket");
		boutonModifDevis =new JButton(" Modification de devis "); 
		labelUser = new JLabel (" Responsable connect� :"+user.getNom() +""+user.getPrenom())  ;
		
		// introduire une espace constant
		containerPanel.add(Box.createRigidArea(new Dimension(0, 50)));
					
		// ajout des composants graphiques 
					
					containerPanel.add(Box.createHorizontalBox());
					System.out.println(user.getNom());
					labelUser.setFont(new Font("Responsable connect�:  \t "+user.getNom()+" "+user.getPrenom() , Font.BOLD, 20));
					containerPanel.add(labelUser) ;
					
					containerPanel.add(Box.createRigidArea(new Dimension(0, 50)));
					containerPanel.add(boutonTrtTkt);
					containerPanel.add(Box.createRigidArea(new Dimension(80, 40)));
					
					containerPanel.add(Box.createRigidArea(new Dimension(0, 40)));
					containerPanel.add(boutonVldSaisie);
					containerPanel.add(Box.createRigidArea(new Dimension(80, 40)));
					
					containerPanel.add(Box.createRigidArea(new Dimension(80, 40)));
					containerPanel.add(boutonCloTkt);
					containerPanel.add(Box.createRigidArea(new Dimension(80, 40)));
					
					containerPanel.add(Box.createRigidArea(new Dimension(80, 40)));
					containerPanel.add(boutonModifDevis);
					containerPanel.add(Box.createRigidArea(new Dimension(80, 40)));
	
					
		this.setContentPane(containerPanel);			
		this.setVisible(true);
		
		
	}

	/**
	 * g�re les actions sur les boutons 
	 */
	public void actionPerformed(ActionEvent e) {
		
	}
	

}
