import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
/**
 * D�finit les diff�rents param�tres pour la fenetre de la connexion client
 * @author Romain DEVAUX
 * @version 1.0
 */
public class ConnexionClient2 extends JFrame implements ActionListener {
	Container contenu;
/**
 * Création de la légende
 */	
	JLabel L1=new JLabel("Code APE");
/**
 * Création de la zone à completer
 */	
	JTextField codeAPE=new JTextField();
/**
 * Création des boutons
 */
	JButton bouton=new JButton("Connexion");
	JButton bouton1=new JButton("Retour");
/**
 * Constructeur et création de la fenêtre
 */
	public ConnexionClient2(){
		this.setBounds(100,100,600,400);
		this.setTitle("interface identification client");
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		contenu=this.getContentPane();
		contenu.setLayout(null);
/**
 * Ajout et dimensionnement des paramètres sur la fenêtre
 */
		contenu.add(L1);
		contenu.add(codeAPE);
		contenu.add(bouton);
		contenu.add(bouton1);
		
		L1.setBounds(40,80,100,50);
		codeAPE.setBounds(130,80,300,50);
		
		bouton.setBounds(250,250,100,50);
		bouton1.setBounds(350,250,100,50);
/**
 * Actionnement des boutons
 */
		bouton1.addActionListener(this);
	}
/**
 * Fonctions mises en place après un clic sur un bouton
 */		
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		this.dispose();
		
		if (e.getSource()==bouton1){
			ChoixClient ChoixClient=new ChoixClient();
		}
		if (e.getSource()==bouton){
			
		}
	}
	
	

}
