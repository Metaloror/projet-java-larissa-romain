import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
/**
 * D�finit les diff�rents param�tres pour la fenetre du responsable
 * @author Romain DEVAUX
 * @version 1.0
 */
public class FenResponsable extends JFrame implements ActionListener {
	Container contenu;
/**
 * Création des boutons
 */
	JButton bouton=new JButton("Saisir Client");
	JButton bouton1=new JButton("Recherche Client");
	JButton bouton2=new JButton("Saisir Ticket");
	JButton bouton3=new JButton("Valider saisie Maintenance");
	
/**
 * Constructeur et création de la fenêtre
 */
	
	public FenResponsable(){
		this.setBounds(100,100,600,400);
		this.setTitle("Welcome");
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		contenu=this.getContentPane();
		contenu.setLayout(null);
/**
 * Ajout et dimensionnement des paramètres sur la fenêtre
 */
		contenu.add(bouton);
		contenu.add(bouton1);
		contenu.add(bouton2);
		contenu.add(bouton3);
		
		bouton.setBounds(130,90,180,50);
		bouton1.setBounds(320,90,180,50);
		bouton2.setBounds(130,180,180,50);
		bouton3.setBounds(320,180,180,50);
/**
 * Actionnement des boutons
 */		
		bouton.addActionListener(this);
		bouton2.addActionListener(this);
		
	}
/**
 * Fonctions mises en place après un clic sur un bouton
 */	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		this.dispose();
		if (e.getSource()==bouton){
			SaisirClient2 SaisieClient2=new SaisirClient2();
		}
		if (e.getSource()==bouton2){
			SaisieTicket2 SaisieTicket2=new SaisieTicket2();
		}
		
	}
	

}
