import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
/**
 * D�finit les diff�rents param�tres pour la fenetre de ChoixClient
 * @author Romain DEVAUX
 * @version 1.0
 */
public class ChoixClient extends JFrame implements ActionListener {
	Container contenu;
	
/**
 * Création des boutons
 */
	JButton bouton=new JButton("Nouveau client");
	JButton bouton1=new JButton("Client existant");
	JButton bouton2=new JButton("Retour");
	
/**
 * Constructeur et création de la fenêtre
 */
	public ChoixClient(){
		this.setBounds(100,100,600,400);
		this.setTitle("Welcome");
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		contenu=this.getContentPane();
		contenu.setLayout(null);
		
/**
 * Ajout des paramètres sur la fenêtre
 */
		contenu.add(bouton);
		contenu.add(bouton1);
		contenu.add(bouton2);
/**
 * Placement et dimensionnement des paramètres sur la fenêtre
 */	
		bouton.setBounds(230,90,150,50);
		bouton1.setBounds(230,185,150,50);
		bouton2.setBounds(230,280,150,50);
		
/**
 * Actionnement des boutons
 */		
		bouton.addActionListener(this);
		bouton1.addActionListener(this);
		bouton2.addActionListener(this);
		
	}
/**
 * Fonctions mises en place après un clic sur un bouton
 */
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		this.dispose();
		if (e.getSource()==bouton){
			SaisirClient2 SaisirClient2=new SaisirClient2();
		}
		if (e.getSource()==bouton1){
			ConnexionClient2 ConnexionClient2=new ConnexionClient2();
		}
		if (e.getSource()==bouton2){
			GMAO GMAO=new GMAO();
		}
	}
	

}
