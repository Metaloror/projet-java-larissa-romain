import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
/**
 * D�finit les diff�rents param�tres pour la fenetre de saisie d'un ticket
 * @author Romain DEVAUX
 * @version 1.0
 */
public class SaisieTicket2 extends JFrame implements ActionListener {
	Container contenu;
/**
 * Création des legendes
 */	
	JLabel L1=new JLabel("Nom demande");
	JLabel L2=new JLabel("Type Maintenance");
	JLabel L3=new JLabel("Lieu");
	JLabel L4=new JLabel("Description");
	JLabel L5=new JLabel("Traitement du ticket");
	JLabel L6=new JLabel("Durée de l'intervention");
/**
 * Création des zones à completer
 */
	
	JTextField nomDemande=new JTextField();
	JTextField typeMaintenance=new JTextField();
	JTextField lieu=new JTextField();
	JTextField description=new JTextField();
	JTextField dureeIntervention=new JTextField();
	
/**
 * Création des boutons
 */
	JButton bouton=new JButton("valider");
	JButton bouton2=new JButton("Creer devis");
	JButton bouton3=new JButton("Retour");
/**
 * Constructeur et création de la fenêtre
 */
	public SaisieTicket2(){
		this.setBounds(100,100,600,400);
		this.setTitle("Traitement du ticket");
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		contenu=this.getContentPane();
		contenu.setLayout(null);
/**
 * Ajout et dimensionnement des paramètres sur la fenêtre
 */
		contenu.add(L1);
		contenu.add(L2);
		contenu.add(L3);
		contenu.add(L4);
		contenu.add(L5);
		contenu.add(L6);
		
		
		
		contenu.add(nomDemande);
		contenu.add(typeMaintenance);
		contenu.add(lieu);
		contenu.add(description);
		contenu.add(bouton);
		contenu.add(bouton2);
		contenu.add(dureeIntervention);
		contenu.add(bouton3);
		
		L5.setBounds(250,0,150,50);
		
		L1.setBounds(40,105,100,50);
		nomDemande.setBounds(150,120,100,20);
		
		L2.setBounds(10,165,150,50);
		typeMaintenance.setBounds(350,120,100,20);
		
		L3.setBounds(280,105,100,50);
		lieu.setBounds(150,180,100,20);
		
		L4.setBounds(270,165,100,50);
		description.setBounds(350,180,100,20);
		
		L6.setBounds(5,45,150,50);
		dureeIntervention.setBounds(150,60,100,20);
		
		bouton.setBounds(250,300,100,50);
		
		bouton2.setBounds(250,230,100,50);
		bouton3.setBounds(350,300,100,50);
/**
 * Actionnement des boutons
 */
		bouton3.addActionListener(this);
		bouton.addActionListener(this);
	}
/**
 * Fonctions mises en place après un clic sur un bouton
 */	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		this.dispose();
			if (e.getSource()==bouton){
				ListeTickets2 ListeTickets2= new ListeTickets2();
				
			}
			if (e.getSource()==bouton3){
				FenResponsable FenResponsable=new FenResponsable();
			}
		}
	}



