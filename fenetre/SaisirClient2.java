import java.awt.*;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * D�finit les diff�rents param�tres pour la fenetre de saisie client
 * @author Romain DEVAUX
 * @version 1.0
 */
public class SaisirClient2 extends JFrame implements ActionListener {
	Container contenu;
/**
 * Création des légendes
 */	
	JLabel L1=new JLabel("Nom Entreprise");
	JLabel L2=new JLabel("Code APE");
	JLabel L3=new JLabel("Numéro SIRET");
	JLabel L4=new JLabel("Adresse");
	JLabel L5=new JLabel("Saisie du client");
	JLabel L6=new JLabel("eMail");
	JLabel L7=new JLabel("N°Telephone");
	
/**
 * Création des zones completer
 */
	JTextField entreprise=new JTextField();
	JTextField ape=new JTextField();
	JTextField siret=new JTextField();
	JTextField adresse=new JTextField();
	JTextField Mail=new JTextField();
	JTextField Telephone=new JTextField();
	
/**
 * Création des boutons
 */	
	
	JButton bouton=new JButton("Ajouter");
	JButton bouton1=new JButton("Retour");
	
/**
 * Constructeur et création fenêtre
 */
	public SaisirClient2(){
		this.setBounds(100,100,600,400);
		this.setTitle("saisie du client");
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		contenu=this.getContentPane();
		contenu.setLayout(null);
/**
 * Ajout des paramètres sur la fenêtre
 */
		contenu.add(L1);
		contenu.add(L2);
		contenu.add(L3);
		contenu.add(L4);
		contenu.add(L5);
		contenu.add(L6);
		contenu.add(L7);
		
	
		
		contenu.add(entreprise);
		contenu.add(ape);
		contenu.add(siret);
		contenu.add(adresse);
		contenu.add(bouton);
		contenu.add(Mail);
		contenu.add(Telephone);
		contenu.add(bouton1);
/**
 * Placement et dimensionnement des paramètres sur la fenêtre
 */		
		L5.setBounds(250,0,100,50);
		
		L1.setBounds(40,105,100,50);
		entreprise.setBounds(150,120,100,20);
		
		L2.setBounds(45,165,100,50);
		ape.setBounds(350,120,100,20);
		
		L3.setBounds(255,105,100,50);
		siret.setBounds(150,180,100,20);
		
		L4.setBounds(270,165,100,50);
		adresse.setBounds(350,180,100,20);
		
		L6.setBounds(60,45,100,50);
		Mail.setBounds(150,60,100,20);
		
		L7.setBounds(260,45,100,50);
		Telephone.setBounds(350,60,100,20);
		
		
		
		bouton.setBounds(250,250,100,50);
		bouton1.setBounds(250,300,100,50);
		
/**
 * Actionnement des boutons
 */
		bouton1.addActionListener(this);
		
		
	}
/**
 * Fonctions mises en place après un clic sur un bouton
 */
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		this.dispose();
		if (e.getSource()==bouton1){
			ChoixClient ChoixClient=new ChoixClient();
			
		}
		
	}

}
