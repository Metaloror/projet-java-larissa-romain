import java.awt.*;
import javax.swing.*;
/**
 * D�finit les diff�rents param�tres pour la fenetre de la liste des tickets
 * @author Romain DEVAUX
 * @version 1.0
 */

public class ListeTickets2 extends JFrame{
	Container contenu;
/**
 * Création des légendes
 */	
	JLabel L1=new JLabel("Ticket 1");
	JLabel L2=new JLabel("Ticket 2");
	JLabel L3=new JLabel("Ticket 3");
	JLabel L4=new JLabel("Liste des tickets non validés");
/**
 * Création des boutons
 */
	JButton bouton=new JButton("Traiter");
	JButton bouton2=new JButton("Traiter");
	JButton bouton3=new JButton("Traiter");
/**
 * Constructeur et création de la fenêtre
 */
	
	public ListeTickets2(){
		this.setBounds(100,100,600,400);
		this.setTitle("Liste des Tickets non validés");
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		contenu=this.getContentPane();
		contenu.setLayout(null);
/**
 * Ajout et dimensionnement des paramètres sur la fenêtre
 */
		contenu.add(L1);
		contenu.add(L2);
		contenu.add(L3);
		contenu.add(L4);
		
		contenu.add(bouton);
		contenu.add(bouton2);
		contenu.add(bouton3);
		
		L1.setBounds(230,45,100,50);
		bouton.setBounds(300,60,100,20);
		
		L2.setBounds(230,105,100,50);
		bouton2.setBounds(300,120,100,20);
		
		L3.setBounds(230,165,100,50);
		bouton3.setBounds(300,180,100,20);
		
		L4.setBounds(250,0,200,50);
		
		
	}
	
	
	
	

}

