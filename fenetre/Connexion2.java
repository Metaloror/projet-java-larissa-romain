import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 * D�finit les diff�rents param�tres pour la fenetre de connexion
 * @author Romain DEVAUX
 * @version 1.0
 */

public class Connexion2 extends JFrame implements ActionListener {
	Container contenu;
/**
 * Création des zones à completer
 */
	
	JLabel L1=new JLabel("login");
	JLabel L2=new JLabel("password");
	JLabel L3=new JLabel("Identification");
/**
 * Création des légendes 
 */
	JTextField identifiant=new JTextField();
	JTextField mdp=new JTextField();
	
/**
 * Création des boutons
 */
	JButton bouton=new JButton("Valider");
	JButton bouton1=new JButton("Retour");
	
/**
 * Constructeur et création fenêtre
 */
	public 	Connexion2(){
		this.setBounds(100,100,600,400);
		this.setTitle("interface identification");
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		contenu=this.getContentPane();
		contenu.setLayout(null);
/**
 * Ajout des paramètres sur la fenêtre
 */
		contenu.add(L1);
		contenu.add(L2);
		contenu.add(L3);
		
		contenu.add(mdp);
		contenu.add(identifiant);
		contenu.add(bouton);
		contenu.add(bouton1);
		
/**
 * Placement et dimensionnement des paramètres sur la fenêtre
 */
		L3.setBounds(250,0,100,50);
		
		L1.setBounds(40,80,100,50);
		identifiant.setBounds(130,80,300,50);
		
		L2.setBounds(40,140,100,50);
		mdp.setBounds(130,140,300,50);
		
		bouton.setBounds(250,250,100,50);
		bouton1.setBounds(350,250,100,50);
		
/**
 * Actionnement des boutons
 */
		bouton1.addActionListener(this);
		bouton.addActionListener(this);
		
/**
 * Fonctions mises en place après un clic sur un bouton
 */
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		this.dispose();
		if (e.getSource()==bouton1){
			GMAO GMAO=new GMAO();
		}
		if (e.getSource()==bouton){
			FenResponsable FenResponsable=new FenResponsable();
			
		}
		
	}
	
	
	

}
