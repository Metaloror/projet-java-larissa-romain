import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class FenOperateur extends JFrame implements ActionListener {
	Container contenu;
/**
 * Création des légendes
 */	
		JLabel L1=new JLabel("Ticket 1");
		JLabel L2=new JLabel("Ticket 2");
		JLabel L3=new JLabel("Ticket 3");
		JLabel L4=new JLabel("Affectation");
/**
 * Création des boutons
 */
		JButton bouton=new JButton("Voir");
		JButton bouton2=new JButton("Voir");
		JButton bouton3=new JButton("Voir");
		JButton bouton4=new JButton("Modifier");
/**
 * Constructeur et création de la fenêtre
 */
		
		public FenOperateur(){
			this.setBounds(100,100,600,400);
			this.setTitle("Liste des Tickets non validés");
			this.setVisible(true);
			this.setDefaultCloseOperation(EXIT_ON_CLOSE);
			contenu=this.getContentPane();
			contenu.setLayout(null);
/**
 * Ajout et dimensionnement des paramètres sur la fenêtre
 */
			contenu.add(L1);
			contenu.add(L2);
			contenu.add(L3);
			contenu.add(L4);
			
			contenu.add(bouton);
			contenu.add(bouton2);
			contenu.add(bouton3);
			contenu.add(bouton4);
			
			
			L1.setBounds(230,45,100,50);
			bouton.setBounds(300,60,100,20);
			
			L2.setBounds(230,105,100,50);
			bouton2.setBounds(300,120,100,20);
			
			L3.setBounds(230,165,100,50);
			bouton3.setBounds(300,180,100,20);
			
			L4.setBounds(250,0,200,50);
			bouton4.setBounds(250,300,100,50);
/**
 * Actionnement des boutons			
 */
			bouton.addActionListener(this);
			bouton2.addActionListener(this);
			bouton3.addActionListener(this);
			bouton4.addActionListener(this);



		}
/**
 * Fonctions mises en place après un clic sur un bouton
 */
		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			this.dispose();
			if (e.getSource()==bouton){
				
			}
			if (e.getSource()==bouton2){
				
			}
			if (e.getSource()==bouton3){
				
			}
}	

}
