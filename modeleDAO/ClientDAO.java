package modeleDAO;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import modele.Client;

/**
 * Class intervenant dans l'ajout et la r�cup�ration des donn�es d'un  client � partir de la BDD
 * @author Larissa AFFOLABI
 * @version 1.2
 */
public class ClientDAO {
	
	/**
	 * Param�tres de connexion � la base de donn�es oracle URL, LOGIN et PASS
	 * sont des constantes
	 */
	final static String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	final static String LOGIN = "GMAO";  
	final static String PASS = "seigneurjesus"; 

	
	/**
	 * Constructeur de la classe
	 */
	public ClientDAO() {
		// chargement du pilote de bases de donn�es
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.err.println("Impossible de charger le pilote de BDD, n' oubliez pas d'importer le fichier .jar dans le projet");
		}

	}

	/**
	 * Permet d'ajouter un client dans la table Client. 
	 * Le mode est auto-commit: Par d�faut chaque insertion est valid�e
	 * @param client le client � ajouter
	 * @return retourne le nombre de lignes ajout�es dans la table
	 */
	public int ajouter(Client client) {
		Connection con = null;
		PreparedStatement ps = null;
		int retour = 0;

		// connexion � la base de donn�es
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
           /* Pr�paration de l'instruction SQL
            * Chaque ? repr�sente une valeur � communiquer dans l'insertion:
            * Les getters permettent de r�cup�rer les valeurs des attributs souhait�s 
            */ 
			ps = con.prepareStatement("INSERT INTO Client (nom_entreprise, clt_adresse, clt_siret, clt_ape, clt_email, clt_phone) VALUES ( ?, ?, ?, ?, ?, ?)") ;
			ps.setString (1, client.getNomEntreprise());
			ps.setString (2, client.getAdresse());
			ps.setInt (3, client.getSiret());
			ps.setString (4, client.getApe());
			ps.setString (5, client.getEmail());
			ps.setInt (6, client.getTelephone());
			
			
		// Ex�cution de la requ�te
			retour = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;
	}
	
	/**
	 * Permet de r�cup�rer un client � partir de son code ape
	 * @param ape le code ape du client
	 * @return 	le client trouv�;
	 * 			null si aucun client ne correspond au code rentr� 
	 */
	public Client getClient(int siret) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Client retour = null;

		// connexion � la base de donn�es
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM Client WHERE CLT_siret = ?");
			ps.setInt(1, siret);

			// on ex�cute la requ�te
			// rs contient un pointeur situ� juste avant la premi�re ligne
			// retourn�e
			rs = ps.executeQuery();
			// passe � la premi�re (et unique) ligne retourn�e
			if (rs.next())
				retour = new Client(rs.getString("nom_Entreprise"),
						rs.getString("clt_adresse"),
						rs.getInt("clt_siret"), rs.getString("clt_ape"), rs.getString("clt_email"), rs.getInt("clt_phone"));

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du ResultSet, du PreparedStatement et de la Connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	/**
	 * Permet de r�cup�rer tous les clients enregistr�s dans la table Client 
	 * @return une ArrayList de clients
	 */
	public ArrayList<Client> getListeClient() {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		ArrayList<Client> retour = new ArrayList<Client>();

		// connexion � la BDD
		try {

			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM Client");

			// on ex�cute la requ�te 
			rs = ps.executeQuery();
			// on parcourt les lignes du r�sultat
			while (rs.next())
				retour.add(new Client (rs.getString("nom_Entreprise"), rs.getString("clt_adresse"), 
						rs.getInt("clt_siret"), rs.getString("clt_ape"), rs.getString("clt_email"), 
						rs.getInt("clt_phone")) );

		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du rs, du preparedStatement et de la connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

}
