package modeleDAO;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import modele.* ;
/**
 * classe intervenant dans les op�rations li�es aux tickets de maintenance 
 * @author Larissa AFFOLABI
 * @version 1.2
 */
public class TicketDAO {
	
	/**
	 * Param�tres de connexion � la base de donn�es oracle URL, LOGIN et PASS
	 * sont des constantes
	 */
	final static String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	final static String LOGIN = "GMAO";  
	final static String PASS = "seigneurjesus"; 

	
	/**
	 * Constructeur de la classe
	 */
	public TicketDAO() {
		// chargement du pilote de bases de donn�es
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.err.println("Impossible de charger le pilote de BDD, n' oubliez pas d'importer le fichier .jar dans le projet");
		}

	}
	
	/**
	 * Permet d'ajouter un ticket dans la table ticket. 
	 * Le mode est auto-commit: Par d�faut chaque insertion est valid�e
	 * @param ticket le ticket � cr�er 
	 * @return retourne le nombre de lignes ajout�es dans la table
	 */
	public int ajouter(Ticket ticket) {
		Connection con = null;
		PreparedStatement ps = null;
		int retour = 0;

		// connexion � la base de donn�es
		try {

			// tentative de connexion
			
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
           /* Pr�paration de l'instruction SQL
            * Chaque ? repr�sente une valeur � communiquer dans l'insertion:
            * Les getters permettent de r�cup�rer les valeurs des attributs souhait�s 
            */ 
			ps = con.prepareStatement("INSERT INTO Ticket (id_client, nom_demande, lieu, tkt_description,traitement,evolution, type_mtn) VALUES (?, ?, ?, ?, ?, ?, ?)") ;
			ps.setInt(1, ticket.getIdClt()) ;
			ps.setString (2, ticket.getNomDemande());
			ps.setString (3, ticket.getLieu());
			ps.setString (4, ticket.getDescription());
			ps.setBoolean (5, ticket.getTraitement());
			ps.setBoolean (6, ticket.getEvolution());
			ps.setString (7, ticket.getType());
			
			
		// Ex�cution de la requ�te
			retour = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;
	}



}
