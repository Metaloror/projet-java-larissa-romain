package modeleDAO;
import modele.*  ;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
/**
 * Class intervenant lors de la connexion d'un client 
 * @author Larissa AFFOLABI
 * @version 1.4
 */
public class ConnexionClientDAO {

	
    /**
	 * Param�tres de connexion � la base de donn�es oracle URL, LOGIN et PASS
	 * sont des constantes
	 */
	final static String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	final static String LOGIN = "GMAO";  
	final static String PASS = "seigneurjesus"; 

	
	/**
	 * Constructeur de la classe
	 */
	public ConnexionClientDAO() {
		// chargement du pilote de bases de donn�es
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.err.println("Impossible de charger le pilote de BDD, n' oubliez pas d'importer le fichier .jar dans le projet");
		}

	  }
	/**	
	 * permet de v�rifier le num�ro de siret de l'entreprise  
	 * @param siret le num�ro de siret de l'entreprise 
	 * @return retour le r�sultat de la v�rification:
	 *                                                1 connexion r�ussie: affichage interface client  
	 *                                               -1 num�ro de siret erron�
	 */
	public Client connecter( int siret ) {
		

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Client retour=null ;

		// connexion à la base de données
		try {
              
            con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM Client WHERE clt_siret=? ");
			ps.setInt( 1 , siret) ;

			// on exécute la requête
			rs = ps.executeQuery();
			// passe à la première (et unique) ligne retournée
               if ( rs.next()  )
               {  
            	   retour = new Client(rs.getString("nom_Entreprise"),rs.getString("clt_adresse"),rs.getInt("clt_siret"), 
            			   rs.getString("clt_ape"), rs.getString("clt_email"), rs.getInt("clt_phone"));
                  retour.setId(rs.getInt("id_client"));
               }
              
 
		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du ResultSet, du PreparedStatement et de la Connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			  }
			try {
				if (ps != null)
					ps.close();
			 } catch (Exception ignore) {
			    }
			  try {
				if (con != null)
					con.close();
			   } catch (Exception ignore) {
			       }
		}
		return retour ;

	

	}
		
	}
	
	
	
	
	
	

