package modeleDAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import modele.User;
/**
 * Classe intervenant lors de la connexion d'un employe de l'entreprise 
 * @author Larissa AFFOLABI
 */
public class AuthentificationDAO {
	/**
	 * Param�tres de connexion � la base de donn�es oracle URL, LOGIN et PASS
	 * sont des constantes
	 */
	final static String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	final static String LOGIN = "GMAO";  
	final static String PASS = "seigneurjesus"; 

	
	/**
	 * Constructeur de la classe
	 */
	public AuthentificationDAO() {
		
		// chargement du pilote de bases de donn�es
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.err.println("Impossible de charger le pilote de GMAO ,  importer le fichier .jar dans le projet");
		}
		
	
	}
	
	/**
	 * permet de v�rifier l'identifiant et le mot de passe de l'utilisateur 
	 * @param login l'identifiant de l'utilisateur
	 * @param password le mot de passe
	 * @return retour le r�sultat de la v�rification: 2 authentification r�ussie: affichage op�rateur
	 *                                                1 authentification r�ussie: affichage responsable 
	 *                                               -1 mot de passe erron�
	 *                                               -2 login erron� 
	 */
	public int authentification (String login, String password) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int retour = 0;

		// connexion à la base de données
		try {
              
            con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT mot_de_passe,type_user FROM User_gmao INNER JOIN Type_user "
					+ "ON User_gmao.id_type_user=Type_user.id_type_user "
					+ "WHERE login=? ");
			ps.setString(1, login);

			// on exécute la requête
			rs = ps.executeQuery();
			// passe à la première (et unique) ligne retournée
               if ( rs.next()  )
               {  
              	   if ( password.equals(rs.getString("mot_de_passe")) ) {
              		   if ( "Op�rateur".equals( rs.getString("type_user") ) )
              			   retour=2  ; // authentification r�ussie 
              		   if ( "Responsable".equals( rs.getString("type_user") ) )
              			   retour=1 ;
              	   } else {
              		   retour=-1 ; // Mot de passe erron�
              	   }
               }
               else
               { 
            	  retour=-2 ; // Login erron�
            			  }
 
			
		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du ResultSet, du PreparedStatement et de la Connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;

	}

	/**
	 * permet de r�cup�rer les informations sur l'utilisateur 
	 * @param login l'identifiant de l'utilisateur
	 * @param password le mot de passe
	 * @return retour toutes les informations de l'utilisateur                             
	 */
	public User getRespo (String login, String password) {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		User retour = null;

		// connexion à la base de données
		try {
              
            con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT * FROM User_gmao  WHERE login=? AND mot_de_passe=?");
			ps.setString(1, login);
			ps.setString(2, password);
			
			// on exécute la requête
			rs = ps.executeQuery();
			// passe à la première (et unique) ligne retournée
			
               if ( rs.next()  )
               {  
              	  retour= new User ( rs.getString("user_nom"), rs.getString("user_prenom"), rs.getInt("id_user"), 
              			  rs.getString("mot_de_passe") ) ;
              	  
               }
              
			
		} catch (Exception ee) {
			ee.printStackTrace();
		} finally {
			// fermeture du ResultSet, du PreparedStatement et de la Connexion
			try {
				if (rs != null)
					rs.close();
			} catch (Exception ignore) {
			}
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour ;

	}

	

	


}
