package modeleDAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;

import modele.Client;

public class FicheMaintenanceDAO {
	
	/**
	 * Param�tres de connexion � la base de donn�es oracle URL, LOGIN et PASS
	 * sont des constantes
	 */
	final static String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	final static String LOGIN = "GMAO";  
	final static String PASS = "seigneurjesus"; 

	
	/**
	 * Constructeur de la classe
	 */
	public FicheMaintenanceDAO() {
		// chargement du pilote de bases de donn�es
		try {
			Class.forName("oracle.jdbc.OracleDriver");
		} catch (ClassNotFoundException e) {
			System.err.println("Impossible de charger le pilote de BDD, n' oubliez pas d'importer le fichier .jar dans le projet");
		}

	}

	/**
	 * ajouter une fiche de maintenance 
	 */
	public int ajouter(Client client) {
		Connection con = null;
		PreparedStatement ps = null;
		int retour = 0;

		// connexion � la base de donn�es
		try {

			// tentative de connexion
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			
           /* Pr�paration de l'instruction SQL
            * Chaque ? repr�sente une valeur � communiquer dans l'insertion:
            * Les getters permettent de r�cup�rer les valeurs des attributs souhait�s 
            */ 
			ps = con.prepareStatement("INSERT INTO Fiche_Maintenance (id_maintenance, nom_mtn, description_mtn, lieu_mtn, date_fiche, , clt_phone) VALUES (Client_sequence.nextval, ?, ?, ?, ?, ?, ?)") ;
			ps.setString (1, client.getNomEntreprise());
			ps.setString (2, client.getAdresse());
			ps.setInt (3, client.getSiret());
			ps.setString (4, client.getApe());
			ps.setString (5, client.getEmail());
			ps.setInt (6, client.getTelephone());
			
			
		// Ex�cution de la requ�te
			retour = ps.executeUpdate();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		return retour;
	}
	

}
