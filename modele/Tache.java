package modele;

/**
 * D�finit les caract�ristiques d'une t�che 
 * @author Larissa AFFOLABI
 * @version 1.1
 */
public class Tache {
	
	/**
	 * nom de la t�che
	 */
	private String designation ;
	/**
	 * le prix HT de la t�che 
	 */
	private double prixHT ;
	/**
	 * le prix TTC de la t�che
	 */
	private double prixTTC ;

	
/**
 * Constructeur
 * @param designation la d�signation de la t�che
 * @param prixHT le prix HT
 * @param prixTTC le prix TTC
 */
	public Tache(String designation, double prixHT, double prixTTC, int quantite){
		this.designation=designation ;
		this.prixHT=prixHT ;
		this.prixTTC=prixTTC ;
		
	}
	
/**
 * R�cup�re la d�signaton de la t�che
 * @return designation
 */
	public String getDesignation(){
		return designation ;
	}
/**
 * R�cup�re le prix TTC de la t�che 
 * @return prixTTC 
 */
	public double getPrixTTC() {
		return prixTTC ;
	}
/**
 * R�cup�re le prix HT de la t�che 
 */
	public double getPrixHT() {
		return prixHT ;
	}

/**
 * Modifie la d�signation actuelle de la t�che
 * @param designation la nouvelle designation
 */
	public void setDesignation( String designation){
		this.designation=designation ;
	}
/**
 * Modifie le prix HT actuel de la t�che
 * @param prixHT le nouveau prix HT
 */
	public void setPrixHT( double prixHT){
		this.prixHT=prixHT ;
		}
/**
 * Modifie le prix TTC actuel de la t�che
 * @param prixTTC le nouveau prix TTC
 */
	public void setPrixTTC( double prixTTC){
			this.prixTTC=prixTTC ;
			}


}
