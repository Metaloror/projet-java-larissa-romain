package modele;
/**
 * D�finit les propri�t�s propre � l'utilisateur responsable
 * @author Larissa AFFOLABI
 * @version 1.0 
 */
public class Responsable extends User {
	
	
    /**
     * Constructeur 
     * @param nom le nom du responsable
     * @param prenom le pr�nom du responsable 
     * @param identifiant l'identifiant du responsable 
     * @param password le mot de passe du responsable
     */
	public Responsable(String nom, String prenom, int identifiant, String password) {
		super(nom, prenom, identifiant, password);
	}

}
