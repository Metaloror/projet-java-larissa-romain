package modele;

/***
 * D�finit les caract�ristiques communes aux utilisateurs du logiciel
 * @author Larissa AFFOLABI
 * @version 1.0
 */
public class User {
	
	/**
	 * le nom de l'utilisateur
	 */
	protected String nom ;
	/**
	 * le pr�nom de l'utilisateur
	 */
	protected String prenom ;
	/**
	 * l'identifiant de l'utilisateur
	 */
	protected int identifiant ;
	/**
	 * le mot de passe de l'utilisateur 
	 */
	protected String password ;
	
	
	
/**
 * Constructeur
 * @param nom le nom
 * @param prenom le pr�nom
 * @param identifiant l'identifiant
 * @param password le mot de passe 
 */
	public User (String nom, String prenom, int identifiant, String password){
		this.nom=nom ;
		this.prenom=prenom ;
		this.identifiant=identifiant ;
		this.password=password ;
	}
/**
 * R�cup�re le nom de l'utilisateur
 * @return nom
 */
	public String getNom(){
		return nom ;
	}
/**
 * R�cup�re le prenom de l'utilisateur
 * @return prenom
 */
	public String getPrenom(){
		return prenom ;
	}
/**
 * R�cup�re l'identifiant de l'utilisateur
 * @return identifiant
 */
   public int getIdentifiant(){
	   return identifiant ;
	}
/**
 * R�cup�re le mot de passe de l'utilisateur
 * @return password le mot de passe
 */
   	public String getPassword(){
   		return password;
   	}
/**
 * Modifie le nom de l'utilisateur
 * @param nom le nouveau nom
 */
   	public void setNom( String nom){
   		this.nom=nom ;
   	}
/**
 * Modifie le pr�nom de l'utilisateur
 * @param prenom le nouveau pr�nom
 */
   public void setPrenom( String prenom){
   	   	this.prenom=prenom ;
   	}
/**
 * Modifie l'identifiant de l'utilisateur
    * @param identifiant le nouvel identifiant
    */
      	public void setIdentifiant( int identifiant){
      		this.identifiant=identifiant ;
     }
/**
 * Modifie le mot de passe de l'utilisateur	
 * @param password nouveau mot de passe 
 */
   public void setPassword(String password) {
    		this.password = password;
    }
}
