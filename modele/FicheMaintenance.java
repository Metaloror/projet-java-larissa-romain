package modele;
/**
 * D�finit les diff�rents param�tres d'une maintenance
 * @author Larissa AFFOLABI
 * @version 1.4
 */
public class FicheMaintenance {
	
	/**
	 * le nom de la maintenance 
	 */
	private String nomMaintenance ;
	/**
	 * le type de la maintenance 
	 */
	private String typeDemande ;
	/**
	 * l'entreprise 
	 */
	private Client leClient ;
	/**
	 * l'op�rateur affect� � la maintenance 
	 */
	private Operateur loperateur ;
	/**
	 * la description de la maintenance faite par le client par le biais du ticket
	 */
	private String description ;
	/**
	 * le lieu de la maintenance 
	 */
	private String lieu ; 
	/**
	 * la date de r�daction de la fiche
	 */
	private String date ;
	
	/**
	 * Constructeur 
	 * @param nomMaintenance le nom de la maintenance 
	 * @param typeDemande le type de la demande
	 * @param leClient le client 
	 * @param loperateur l'op�rateur affect� 
	 * @param description la description de la maintenance r�dig�e par le client 
	 */
	public FicheMaintenance(String nomMaintenance, String typeDemande, String description,  Client leClient){
		this.nomMaintenance=nomMaintenance ;
		this.typeDemande=typeDemande ;
		this.description=description ;
		this.leClient=leClient ;
		this.loperateur=null  ;
	}
	/**
	 * getter pour l'attribut description
	 * @return description la description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * setter pour l'attribut description
	 * @param description la description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * getter pour l'attribut nom Maintenance
	 * @return nomMaintenance le nom de la maintenance 
	 */
	public String getNomMaintenance() {
		return nomMaintenance;
	}
	/**
	 * setter pour l'attribut nom maintenance
	 * @param nomMaintenance le nom de la maintenance 
	 */
	public void setNomMaintenance(String nomMaintenance) {
		this.nomMaintenance = nomMaintenance;
	}
	/**
	 * getter pour l'attribut type de la demande 
	 * @return typeDemande le type de la demande 
	 */
	public String getTypeDemande() {
		return typeDemande;
	}
	/**
	 * setter pour l'attribut type de la demande 
	 * @param typeDemande le nouveau type de la demande 
	 */
	public void setTypeDemande(String typeDemande) {
		this.typeDemande = typeDemande;
	}
	/**
	 * getter pour l'attribut le client
	 * @return leClient le client 
	 */
	public Client getLeClient() {
		return leClient;
	}
	/**
	 * setter pour l'attribut le client 
	 * @param leClient les nouvelles informations du client 
	 */
	public void setLeClient(Client leClient) {
		this.leClient = leClient;
	}
	/**
	 * getter pour l'attribut op�rateur
	 * @return loperateur l'op�rateur
	 */
	public Operateur getLoperateur() {
		return loperateur;
	}
	/**
	 * setter pour l'attribut op�rateur
	 * @param leClient les nouvelles informations de l'op�rateur 
	 */
	public void setLoperateur(Operateur loperateur) {
		this.loperateur = loperateur;
	}
	/**
	 * getter pour l'atrribut lieu
	 * @return lieu le lieu de la maintenance
	 */
	public String getLieu() {
		return lieu;
	}
	/**
	 * setter pour l'attribut lieu
	 * @param lieu le lieu de la maintenance
	 */
	public void setLieu(String lieu) {
		this.lieu = lieu;
	}
	/**
	 * getter pour l'attribut date
	 * @return date la date
	 */
	public String getDate() {
		return date;
	}
	/**
	 * setter pour l'attribut date
	 * @param date la date 
	 */
	public void setDate(String date) {
		this.date = date;
	}
	

	
}
