package modele;

import java.util.ArrayList;

/**
 * D�finit les propri�t�s propre � l'utilisateur op�rateur
 * @author Larissa AFFOLABI
 * @version 1.0 
 */
public class Operateur extends User{
	
	/**
	 * la liste des maintenances affect�es � l'op�rateur 
	 */
	private ArrayList<FicheMaintenance> listeMaintenance ;
	
	

/**
 *Constructeur
 *@param nom le nom de l'op�rateur
 *@param prenom le pr�nom de l'op�rateur
 *@param identifiant l'identifiant de l'op�rateur
 *@param password le mot de passe de l'op�rateur 
 */
	public Operateur(String nom, String prenom, int identifiant, String password) {
		super(nom, prenom, identifiant, password);
		listeMaintenance = new ArrayList<FicheMaintenance>() ;
	}
	
/**
 * R�cup�re la liste des maintenances affect�es � l'op�rateur
 * @return listeMaintenance
 */
	public ArrayList<FicheMaintenance> getListeMaintenance() {
		return listeMaintenance;
	}


	
	
	

}
