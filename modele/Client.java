package modele;

import java.util.ArrayList;

/**
 * D�finit les diff�rentes informations li�es au client 
 * @author Larissa AFFOLABI
 * @version 1.2
 */
public class Client {
	
	
	/**
	 * nom de l'entreprise
	 */
	private String nomEntreprise ;
	/**
	 * l'adresse de l'entreprise 
	 */
	private String adresse ;
	/**
	 * num�ro de siret de l'entreprise
	 */
	private int siret ;
	/**
	 * code APE de l'entreprise
	 */
	private String ape ;
	/**
	 * L'adresse �lectronique de l'entreprise
	 */
	private String email ;
	/**
	 * le num�ro de t�l�phone 
	 */
	private int telephone ;
	/**
	 * l'identifiant du client 
	 */
	private int id ;
	/**
	 * liste des diff�rentes demande de maintenance faites 
	 */
	private ArrayList<FicheMaintenance> c_listeMaintenance ;
	/**
	 * liste des devis concernant les demandes faites 
	 */
	private ArrayList<Devis> lesDevis;
	
	
/**
 * constructeur
 * @param nomEntreprise le nom de l'entreprise
 * @param adresse l'adresse de l'entreprise
 * @param siret 
 * @param ape le code APE de l'entreprise 
 */
	public Client (String nomEntreprise, String adresse, int siret, String ape, String email, int telephone){
		
		this.nomEntreprise=nomEntreprise ;
		this.adresse=adresse ;
		this.siret=siret ;
		this.ape=ape ;
		this.email=email ;
		this.telephone=telephone ;
		c_listeMaintenance= new ArrayList<FicheMaintenance>() ;
		lesDevis= new ArrayList<Devis>() ;
		
	}
	
/**
 * m�thode permettant de r�cup�rer le nom de l'entreprise
 * @return nomEntreprise le nom de l'entreprise
 */
	public String getNomEntreprise() {
		return nomEntreprise ;
	}
	
/**
 * m�thode permettant de r�cup�rer l'adresse de l'entreprise
 * @return adresse l'adresse de l'entreprise
 */
	public String getAdresse() {
		return adresse ;
		}	

/**
 * m�thode permettant de r�cup�rer le num�ro de siret de l'entreprise
 * @return siret le num�ro de siret de l'entreprise
 */
	
	public int getSiret() {
		return siret ;
		}
	
/**
 * m�thode permettant de r�cup�rer le code APE de l'entreprise
 * @return ape le code APE de l'entreprise
 */
	public String getApe() {
		return ape ;
		}
	
/** m�thode permettant de r�cup�rer la liste des maintenances du client 
 *  @return c_listeMaintenance 
 */
	public ArrayList<FicheMaintenance> getListeMaintenance() {
		return c_listeMaintenance ;
		}
	
/**
 * m�thode permettant de modifier le nom de l'entreprise
 * @param nomEntreprise le nom de l'entreprise
 */
	public void setNomEntreprise(String nomEntreprise) {
		this.nomEntreprise=nomEntreprise ;
		}
		
/**
 * m�thode permettant de modifier l'adresse de l'entreprise
 * @param adresse l'adresse de l'entreprise
 */
   public void setAdresse(String adresse) {
		this.adresse=adresse ;
			}	

/**
 * m�thode permettant de modifier le num�ro de siret de l'entreprise
 * @param siret le num�ro de siret de l'entreprise
 */
	public void setSiret(int siret) {
		this.siret=siret ;
			}
	
/**
 * getter pour l'attribut email 
 */
	public String getEmail() {
		return email;
	}
	
/**
 * setter  pour l'attribut email
 */
	public void setEmail(String email) {
		this.email = email;
	}
	
/**
 * getter pour l'attribut telephone 
 * @return telephone le num�ro de t�l�phone 
 */
	public int getTelephone() {
		return telephone;
	}
	
/**
 * setter pour l'attribut telephone 
 * @param telephone le nouveau num�ro de t�l�phone 
 */
	public void setTelephone(int telephone) {
		this.telephone = telephone;
	}
	
/**
 * m�thode permettant de modifier le code APE de l'entreprise
 * @param ape le nouveau code APE de l'entreprise
 */
   public void setApe(String ape) {
		this.ape=ape ;
			}
   
/**
 * getter de l'attribut id 
 * @return id l'id du client 
 */
   public int getId() {
		return id;
	}
/**
 * setter de l'attribut id 
 * @param id
 */
	public void setId(int id) {
		this.id = id;
	}

 /**
  * m�thode permettant d'afficher les informations du client 
  */
   public void afficher() {
	  
	System.out.println( " Client: "+nomEntreprise +"\n Adresse: "+adresse +"\n Num�ro de SIRET: "+siret +"\n Code APE: "+ape+"\n Email: "+email+"\n T�l�phone: "+telephone); 
	   
   }
   
   
}
