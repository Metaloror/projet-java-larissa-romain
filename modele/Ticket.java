package modele;

/**
 * D�finit les caract�ristiques des tickets
 * @author Larissa AFFOLABI
 * @version 1.2
 */
public class Ticket {
	
	/**
	 * le nom de la demande de maintenance
	 */
	private String nomDemande ;
	/**
	 * l'identifiant du client 
	 */
	private int idClt ;
	/**
	 * la description de la demande 
	 */
	private String description ;
	/**
	 * le lieu d'ex�cution de la maintenance 
	 */
	private String lieu ; 
	/**
	 * le niveau de traitement du ticket pour la saisie de maintenance: false pour aucune saisie de maintenance faite true pour une saisie maintenance faite
	 */
	private boolean traitement ;
	/**
	 * l'�volution du ticket pour le client: false pour ticket en cours de traitement true pour ticket cl�tur� 
	 */
	private boolean evolution ;
	/**
	 * le type de la maintenance 
	 */
	private String type ;
	
/**
 * Constructeur
 * @param nomDemande le nom de la demande
 * @param idClt l'identifiant du client 
 * @param description la description de la demande
 * @param lieu le lieu de la maintenance 
 * @param type le type de la demande 
 */
	public Ticket(String nomDemande,int idClt, String description, String lieu,String type){
		this.nomDemande=nomDemande;
		this.idClt=idClt ;
		this.description=description ;
		this.lieu=lieu ;
		this.type=type ;
		traitement=false ;
		evolution=false ;
	}

/**
 * getter de l'attribut nom de la demande
 * @return nomDemande 
 */
	public String getNomDemande() {
		return nomDemande;
	}
/**
 * Modifie le nom de la demande 
 * @param nomDemande
 */
	public void setNomDemande(String nomDemande) {
		this.nomDemande = nomDemande;
	}
/**
 * getter de l'attribut idClt
 * @return idClt l'identifiant du client 
 */
	public int getIdClt() {
		return idClt ;
	}

/**
 * getter de l'attribut  description de la demande
 * @return description 
 */
	public String getDescription() {
		return description;
	}
/**
 * Modifie la description de la demande 
 * @param description la nouvelle description
 */
	public void setDescription(String description) {
		this.description = description;
	}
/**
 * getter de l'attribut lieu d'intervention de la demande
 * @return lieu 
 */
	public String getLieu() {
		return lieu;
	}
/**
 * Modifie le lieu d'intervention de la demande 
 * @param lieu le nouvel emplacement
	 */
	public void setLieu(String lieu) {
		this.lieu = lieu;
	}
/**
 * getter de l'attribut traitement 
 * @return traitement le niveau de traitement du ticket au niveau du responsable
 */
	public boolean getTraitement() {
		return traitement;
	}
/**
 * setter de l'attribu traitement 
 * @param traitement le nouveau niveau de traitement du ticket au niveau du responsable
 */
	public void setTraitement(boolean traitement) {
		this.traitement = traitement;
	}
/**
 * getter de l'attribut evolution
 * @return l'�volution du ticket pour le client 
 */
	public boolean getEvolution() {
		return evolution;
	}
	
/**
 * setter de l'attribut evolution
 * @param evolution le nouvel �tat d'�volution 
 */
	public void setEvolution(boolean evolution) {
		this.evolution = evolution;
	}
/**
 * getter de l'attribut type	
 * @return type le type de la demande 
 */
	public String getType() {
		return type;
	}
	/**
	 * setter de l'attribut type
	 * @param type le nouveau type de la demande 
	 */
	public void setType(String type) {
		this.type = type;
	}
}
