package modele;

/**
 * Classe reliant une t�che � sa quantit�
 * @author Larissa AFFOLABI
 * @version 1.0
 */
public class TacheQuantite {
	
    /**
     * l'objet t�che 
     */
	private Tache tache ;
	/**
	 * la quantit� de la t�che 
	 */
	private int quantite ;
	
	/**
	 * Constructeur 
	 * @param tache la t�che 
	 * @param quantite la quantit� de la t�che 
	 */
	public TacheQuantite(Tache tache, int quantite) {
		this.tache = tache;
		this.quantite = quantite;
	}
	
	/**
	 * getter pour l'attribut tache 
	 * @return tache la t�che 
	 */
	public Tache getTache() {
		return tache;
	}
	/**
	 * setter de l'attribut tache
	 * @param tache la nouvelle t�che 
	 */
	public void setTache(Tache tache) {
		this.tache = tache;
	}
	/**
	 * getter de l'attribut quantite
	 * @return quantite la quantit� 
	 */
	public int getQuantite() {
		return quantite;
	}
	/**
	 * setter pour l'attribut quantite
	 * @param quantite la nouvelle quantit� 
	 */
	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}

}
