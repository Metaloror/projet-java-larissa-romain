package modele;
/**
 * D�finit les diff�rents param�tres d'une maintenance
 * @author Larissa AFFOLABI
 * @version 1.0
 */
public class Maintenance {
	
	private String nomMaintenance ;
	private String typeDemande ;
	private Client leClient ;
	private Operateur loperateur ; 

}
/**
 * Constructeur
 */

	public Maintenance(String nomMaintenance, String typeDemande, Operateur loperateur){
		this.nomMaintenance=nomMaintenance;
		this.typeDemande=typeDemande;
		this.loperateur=loperateur
		
	}

/**
 * R�cup�re du nom de la maintenance
 * @return nomMaintenance 
 */	
	public String getNomMaintenance(){
		return nomMaintenance;
	}

/**
 * R�cup�re l'opérateur
 * @return loperateur 
 */	
	public Operateur getOperateur(){
		return loperateur;
	}
	
/**
 * R�cup�re le type de la demande
 * @return typeDemande 
 */
	public String getTypeDemande(){
		return typeDemande;
	}
	
/**
 * Modifie le nom de la maintenance
 * @param nomMaintenance 
 */
	public void setNomMaintenance(String nomMaintenance){
		this.nomMaintenance=nomMaintenance;
	}
	
/**
 * Modifie l'operateur
 * @param loperateur 
 */	
	public void setOperateur (operateur Operateur){
		this.loperateur=operateur
	}
	
	
	
	
	