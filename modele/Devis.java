package modele;

import java.util.ArrayList;

/**
 * D�finit les donn�es contenant le devis 
 * @author Larissa AFFOLABI
 * @version 1.1
 */
public class Devis {
	
	
/**
 * date de cr�ation du devis
 */
	private String date ;
/**
 * liste des diff�rentes t�ches transcrits sur le devis avec leur quantite 
 */
	private ArrayList<TacheQuantite> listeTache ;
/**
 * l'entreprise demandant la maintenance 
 */
	private Client leClient ;
	
	
/**
 * Constructeur 
 * @param date la date d'�dition du devis
 * @param numero le num�ro du devis
 * @param leClient l'entreprise � laquelle est destin�e le devis
 */
	public Devis(String date, Client leClient){
		this.date=date ;
		this.leClient=leClient ;
		listeTache= new ArrayList<TacheQuantite>();
		
	}

/**
 * R�cup�re le client correspondant au devis
 * @return leClient 
 */
	public Client getClient(){
		return leClient ;
	}
	
/**
 * R�cup�re le prix total HT des t�ches list�es
 * @return prix total HT
 */
	public double getPrixTotalHT(){
		double prixTotalHT=0.0 ;
	 for(int i=0; i<listeTache.size(); i++)
			prixTotalHT += listeTache.get(i).getTache().getPrixHT()*listeTache.get(i).getQuantite() ;
		
		    return prixTotalHT ;
	}
	
/**
 * R�cup�re le prix total TTC des t�ches list�es
 * @return prix total TTC
 */
   public double getPrixTotalTTC(){
	   double prixTotalTTC=0.0 ;
	for(int i=0; i<listeTache.size(); i++)
		prixTotalTTC += listeTache.get(i).getTache().getPrixTTC()*listeTache.get(i).getQuantite() ;
			
		return prixTotalTTC ;
		}
/**
 * R�cup�re la liste des t�ches
 * @return listeTache 
 */
   public ArrayList<TacheQuantite> getListeTache(){
	   return listeTache ;
   }
 /**
  * Modifie le client
  * @param leClient le nouveau client correspondant au devis
  */
   public void setClient(Client leClient){
	   this.leClient=leClient ;
   }
 
/**
 * Ajoute une nouvelle t�che � la liste 
 * @param tache la nouvelle t�che � ajouter
 */
   public void AjouterTache( Tache tache, int quantite){
	  TacheQuantite tacheQuantite= new TacheQuantite(tache,quantite) ;
	   listeTache.add( tacheQuantite) ;
   }


}
